package lab1;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Visits
 */
@WebServlet("/Visits")
public class Visits extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Cookie cookies[] = request.getCookies();
		
		Cookie counter=null;
		if(cookies != null){
			for(int i=0; i<cookies.length; i++){
				if(cookies[i].getName().equals("counter")){
					counter = cookies[i];
					int times = Integer.parseInt(counter.getValue());
					times++;
					counter.setValue( "" + times );
				}
			}
		}
		if(counter == null){
			counter = new Cookie("counter", "1");
		}
		
		counter.setMaxAge(60*60*24);
		response.addCookie( counter );
		
		PrintWriter out = response.getWriter();
		out.println("<h1>You've visited us "+ counter.getValue() +" time already</h1>");
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
