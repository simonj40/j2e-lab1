package lab1;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class ListItems
 */
@WebServlet("/ListItems")
public class ListItems extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ListItems() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("items.html");	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		response.setContentType("text/html");
		HttpSession session = request.getSession(true);
		ArrayList<String> items = (ArrayList<String>)session.getAttribute("items");
		String item = request.getParameter("newItem");
		if( items == null ){
			items = new ArrayList<String>();
		}
		if(!item.isEmpty()){
			items.add(item);
		}
		session.setAttribute("items", items);
		
		out.println("List of items:");
		out.println("<ul>");
		for(int i=0; i< items.size(); i++){
			int counter = 0;
			for(String x : items ){
				if(x.equals(items.get(i))) counter++;
			}
			out.println("<li>"+items.get(i)+" (present "+counter+" time in the list)</li>");
		}
		out.println("</ul>");
	}

}
