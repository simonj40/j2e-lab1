package lab1;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class ShowSession
 */
@WebServlet("/ShowSession")
public class ShowSession extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ShowSession() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		HttpSession session = request.getSession(true);
		String fullName = request.getParameter("fname");
		String dateOfBirth = request.getParameter("birth");
		if(fullName != null){
			session.setAttribute("fname", fullName);
		}
		if(dateOfBirth != null){
			session.setAttribute("birth", dateOfBirth);
		}
		
		Integer counter = (Integer) session.getAttribute("counter");
		if(counter == null){
			session.setAttribute("counter", 1);
			out.println("<h1>Welcome on my site!</h1>");
		}else{
			counter++;
			session.setAttribute("counter", counter);
			out.println("<h1>Welcome back (You've visited us "+counter+" times already)</h1>");
		}
		String fname = (String)session.getAttribute("fname");
		String birth = (String)session.getAttribute("birth");
		
		if(fname == null || birth == null){
			out.println("<form method='POST' action='ShowSession'>");
			out.println("<label>Full Name</label>");
			out.println("<input type=text name='fname'><br>");
			out.println("<label>Date of Birth</label>");
			out.println("<input name='birth' type='date'>");
			out.println("<input type='submit'>");
			out.println("</form>");
		}else{
			//Date date = new Date(birth);
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date date;
			Integer days = null;
			try {
				date = formatter.parse(birth);
				Calendar birthCalendar = Calendar.getInstance(); //birth date
				Calendar calendar = Calendar.getInstance(); //next birthday
				birthCalendar.setTime(date);
				calendar.set(Calendar.MONTH, birthCalendar.get(Calendar.MONTH));
				calendar.set(Calendar.DAY_OF_MONTH, birthCalendar.get(Calendar.DAY_OF_MONTH));
				//if birthday is this year
				if(Calendar.getInstance().compareTo(calendar)<0){
					double tmp = Calendar.getInstance().compareTo(calendar);
					tmp = tmp/(1000*86400);
					days = (int)tmp;
				}else{
					calendar.set(Calendar.YEAR, birthCalendar.get(Calendar.YEAR)+1);
					double tmp = Calendar.getInstance().compareTo(calendar);
					tmp = tmp/(1000*86400);
					days = (int)tmp;
				}
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(days != null){
				out.println("<h2>Hi, "+ fname +". There are "+days+" days to your birthday.</h2>");
			}else{
				out.println("<h2>Hi, "+ fname +".</h2>");
			}
		}
			
	}
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
