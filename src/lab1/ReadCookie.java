package lab1;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ReadCookie
 */
@WebServlet("/ReadCookie")
public class ReadCookie extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReadCookie() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Cookie cookies[] = request.getCookies();
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<h1>From your cookies</h1>");
		if(cookies != null){
			out.println("<table>");
			out.println("<tr>");
			for(int i=0; i<cookies.length; i++){	
				out.println("<th>"+cookies[i].getName()+"</th>");
			}
			out.println("</tr>");
			out.println("<tr>");
			for(int i=0; i<cookies.length; i++){	
				out.println("<td>"+cookies[i].getValue()+"</td>");
			}
			out.println("</tr>");
			out.println("</table>");
		}else{
			out.println("No cookie found");
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
