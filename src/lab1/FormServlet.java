package lab1;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class FormServlet
 */
@WebServlet("/FormServlet")
public class FormServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.sendRedirect("form.html");		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String fname = request.getParameter("fname");
		String lname = request.getParameter("lname");
		String birth = request.getParameter("birth");
		String sex = request.getParameter("sex");
		//Set Cookie
		Cookie cookie1 = new Cookie("FrstName",fname);
		Cookie cookie2 = new Cookie("LastName",lname);
		Cookie cookie3 = new Cookie("BirthDay",birth);
		Cookie cookie4 = new Cookie("Sex",sex);
		cookie1.setMaxAge(60*60*24);
		cookie2.setMaxAge(60*60*24);
		cookie3.setMaxAge(60*60*24);
		cookie4.setMaxAge(60*60*24);
		response.addCookie( cookie1 );
		response.addCookie( cookie2 );
		response.addCookie( cookie3 );
		response.addCookie( cookie4 );
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<table>");
		out.println("<tr>");
		out.println("<th>First Name</th>");
		out.println("<th>Last Name</th>");
		out.println("<th>Date of Birth</th>");
		out.println("<th>Sex</th>");
		out.println("</tr>");
		out.println("<tr>");
		out.println("<td>"+fname+"</td>");
		out.println("<td>"+lname+"</td>");
		out.println("<td>"+birth+"</td>");
		out.println("<td>"+sex+"</td>");
		out.println("</tr>");
		out.println("</table>");
		
	}

}
